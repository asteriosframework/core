## [1.1.0-rc.9](https://gitlab.com/asteriosframework/core/compare/1.1.0-rc.8...1.1.0-rc.9) (2024-10-26)

### Features

* model insert & update allow null value ([2b6f452](https://gitlab.com/asteriosframework/core/commit/2b6f452a205170c53fd7ad00cefb814892aca201))

## [1.1.0-rc.8](https://gitlab.com/asteriosframework/core/compare/1.1.0-rc.7...1.1.0-rc.8) (2024-08-27)

### Bug Fixes

* **deps:** update dependency firebase/php-jwt to v6.10.1 ([b48472d](https://gitlab.com/asteriosframework/core/commit/b48472d02ce58293594682435f6d508bc7a06df8))

## [1.1.0-rc.7](https://gitlab.com/asteriosframework/core/compare/1.1.0-rc.6...1.1.0-rc.7) (2024-08-26)

### Bug Fixes

* **deps:** update dependency conventional-changelog-conventionalcommits to v8 ([d7232d1](https://gitlab.com/asteriosframework/core/commit/d7232d12bc71196fabc8badd72f504698312297e))

## [1.1.0-rc.6](https://gitlab.com/asteriosframework/core/compare/1.1.0-rc.5...1.1.0-rc.6) (8/25/2024)


### Features

* refactor logger class ([baa4489](https://gitlab.com/asteriosframework/core/commit/baa4489cc47975077b3fa2615dffe5ca79bb0b53))

## [1.1.0-rc.5](https://gitlab.com/asteriosframework/core/compare/1.1.0-rc.4...1.1.0-rc.5) (8/20/2024)


### Bug Fixes

* Bug in reading .env values that are upper and lowercase strings ([da3f690](https://gitlab.com/asteriosframework/core/commit/da3f690b6fd7781d6e273c014c11629443b05c00))

## [1.1.0-rc.4](https://gitlab.com/asteriosframework/core/compare/1.1.0-rc.3...1.1.0-rc.4) (8/18/2024)


### Features

* add hasItems function ([7ee5eb7](https://gitlab.com/asteriosframework/core/commit/7ee5eb7b2edaaf881cfda1142cd9b14273ca2c1d))

## [1.1.0-rc.3](https://gitlab.com/asteriosframework/core/compare/1.1.0-rc.2...1.1.0-rc.3) (8/17/2024)


### Features

* add Cast class ([bfd0953](https://gitlab.com/asteriosframework/core/commit/bfd0953553b200a77aa0beeb165c0164b0c7e361))
* add Cast class ([cf074a0](https://gitlab.com/asteriosframework/core/commit/cf074a0c547c6394ecb8dffcb9f386bd93c25f30))

## [1.1.0-rc.2](https://gitlab.com/asteriosframework/core/compare/1.1.0-rc.1...1.1.0-rc.2) (8/17/2024)


### Features

* add more functions to collection ([1c6d812](https://gitlab.com/asteriosframework/core/commit/1c6d8122075e79bfbbb00295fd21d23ee6a4993c))

## [1.1.0-rc.1](https://gitlab.com/asteriosframework/core/compare/1.0.1-rc.1...1.1.0-rc.1) (8/16/2024)


### Features

* add Collection to asterios core ([941ed31](https://gitlab.com/asteriosframework/core/commit/941ed3108e14c87169968fc7ca5ef882dfbab972))
* add two functions to old db class that are needed by the upcoming asterios CMS. ([146d8e0](https://gitlab.com/asteriosframework/core/commit/146d8e003d5781e2a2358ac26c7ed861c1877652))
* fix new line ([0f380de](https://gitlab.com/asteriosframework/core/commit/0f380de418e96a542809ecf63d490a9ecd91d43e))
* php fixer fix new line ([05b3686](https://gitlab.com/asteriosframework/core/commit/05b36862ff23b1feac1d4faadc58f66d0a68a756))

## [1.0.1-rc.1](https://gitlab.com/asteriosframework/core/compare/1.0.0...1.0.1-rc.1) (12/28/2023)


### Bug Fixes

* **pipeline:** Pipeline error resolved ([9032d09](https://gitlab.com/asteriosframework/core/commit/9032d09b9a001fc628d9e1d22ab105298d331aa9))
