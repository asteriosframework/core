<?php declare(strict_types=1);

namespace Asterios\Core\Interfaces;

interface SingletonInterface
{
    public static function getInstance();
}