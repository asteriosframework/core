<?php

namespace Asterios\Core\Interfaces;

interface Stringable
{
    public function __toString(): string;
}